#!/usr/bin/env bash

iDIR="$HOME/.config/dunst/icons"
notification_timeout=1000

get_brighness() {
	echo $(expr $(brightnessctl g * 5) \* 5)
}

# Get icons
get_icon() {
	current=$(get_brighness)

	if [ "$current" -le "20" ]; then
		echo "$iDIR/brightness-20.png"
	elif [ "$current" -le "40" ]; then
		echo "$iDIR/brightness-40.png"
	elif [ "$current" -le "60" ]; then
		echo "$iDIR/brightness-60.png"
	elif [ "$current" -le "80" ]; then
		echo "$iDIR/brightness-80.png"
	else
		echo "$iDIR/brightness-100.png"
	fi
}

# Notify
notify_user() {
	notify-send -h string:x-dunst-stack-tag:brightness_notif -h int:value:$(get_brighness) -u low -i "$(get_icon)" "Brightness : $(get_brighness)%"
}

# Execute accordingly
case "$1" in
"--get")
	get_backlight
	;;
"--inc")
	brightnessctl s 1+ && notify_user
	;;
"--dec")
	brightnessctl s 1- && notify_user
	;;
*)
	get_backlight
	;;
esac
