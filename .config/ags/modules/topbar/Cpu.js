const iter = 1;
const pool_time = 1.5;

const cpu = Variable(0, {
  poll: [
    pool_time * 1000, `mpstat -n ${Math.floor(iter)} 1`,
    out => parseInt(out.split(/\n/)[4].split(/\s+/)[2])
  ]
});

function pad(n) {
  return n.toString().padStart(3, '_');
}

export default () => Widget.Label({
  label: cpu.bind().transform(p => `󰍛 ${pad(p)}%`),
  class_name: 'cpu'
});
