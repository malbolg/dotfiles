
const ram = Variable(-1, {
  poll: [2000, 'free', out => {

    return Math.ceil(
      parseInt(out.split(/\n/)[1].split(/\s+/)[2]) / 100000
    ) / 10;
  }],
})

const NumberRam = () => Widget.Label({
  label: ram.bind().transform(p => `󰾆 ${p.toFixed(1)}G`),
  class_name: 'ram'
})

export default () => NumberRam(); 
