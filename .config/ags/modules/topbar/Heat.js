import Utils from 'resource:///com/github/Aylur/ags/utils.js'

let sensors = {
  high: 9999,
  crit: 9999
};

Utils.execAsync('sensors')
  .then((out) => {
    let ln = out.split(/\n/)[3].split(/\s+/)

    sensors['high'] = parseInt(ln[5])
    sensors['crit'] = parseInt(ln[8])
  })

const heat = Variable(0, {
  poll: [6000, 'sensors',
    out => parseInt(out.split(/\n/)[3].split(/\s+/)[2])
  ]
})

function get_class(heat, high, crit) {
  if (heat < high)
    return ''

  if (heat < crit)
    return 'high'

  return 'crit'
}


export default () => Widget.Label({
  label: heat.bind().transform(t => ` ${t}°C`),
  class_name: heat.bind()
    .transform(t => `heat ${get_class(t, sensors['high'], sensors['crit'])}`)
})
