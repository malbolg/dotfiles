import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js';

/** @param {number} id */
const WorkspaceButton = (id) => Widget.EventBox({
  class_name: Hyprland.active.workspace.bind('id')
    .transform(n => `${n === id ? 'focus' : ''}`),
  on_primary_click_release: () => Hyprland.sendMessage(`dispatch workspace ${id}`),
  child: Widget.Label({
    label: `${id}`,
    class_name: "ws-num"
  })
});

export default () => Widget.Box({
  spacing: 2,
  class_name: 'workspaces',
  children: Hyprland.bind('workspaces').transform(ws => {
    return ws
      .sort((lhs, rhs) => lhs.id > rhs.id)
      .map(({ id }) => WorkspaceButton(id))
  })
});
