import { execAsync } from 'resource:///com/github/Aylur/ags/utils.js';


/**
 * @param {string} format
*/
export default (format) => Widget.Label({
  class_name: 'clock',
  setup: self => self
    .poll(1000, self => execAsync(['date', format]) // %d %b
      .then(date => self.label = ` ${date}`)
    )
})


