// User modules
import Workspaces from "./Workspaces.js"
import Clock from "./Clock.js"
import Separator from "../common/Separator.js"
import Battery from "./Battery.js"
import Cpu from "./Cpu.js"
import Ram from "./Ram.js"
import Heat from "./Heat.js"
import Power from './Power.js'
// ------------


// Set up anchors on bar
const Left = () => Widget.Box({
  children: [
    Workspaces(),
    Separator(),
  ],

  class_name: 'bar_left',
});

const Center = () => Widget.Box({
  children: [

  ],

  class_name: 'bar_center',
});

const Right = () => Widget.Box({
  children: [
    Separator(),
    Heat(),
    Cpu(),
    Ram(),
    Battery(),
    Separator(),
    Clock("+%H:%M:%S"),
    Separator(),
    Power()
  ],

  class_name: 'bar_right',
  hpack: 'end',
});
// ----


// Create topbar
export default () => Widget.Window({
  name: 'hello!',
  anchor: ['top', 'left', 'right'],
  exclusivity: 'exclusive',
  layer: 'bottom',
  class_name: 'bar',

  child: Widget.CenterBox({
    class_name: 'bar_wrapper',

    startWidget: Left(),
    centerWidget: Center(),
    endWidget: Right()
  })
});
// ----


