import App from 'resource:///com/github/Aylur/ags/app.js'
import { POWERMENU_WINDOW_NAME } from '../windows/power/main.js'

console.log(App.windows)
export default () => Widget.EventBox({
  child: Widget.Label({ label: '⏻', class_name: 'power-button' }),
  on_primary_click: () => App.toggleWindow(POWERMENU_WINDOW_NAME)
})


