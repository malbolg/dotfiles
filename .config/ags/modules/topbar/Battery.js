import Battery from 'resource:///com/github/Aylur/ags/service/battery.js'
import App from 'resource:///com/github/Aylur/ags/app.js';

function get_icon(percent, charging) {
  if (charging)
    return `${App.configDir}/assets/ico/battery/battery-charging.svg`

  return `${App.configDir}/assets/ico/battery/battery-${Math.ceil(percent / 5) * 5}.svg`
}

function colorise(percent) {
  if (percent > 40)
    return 'high'

  if (percent > 15)
    return 'med'

  return 'low'
}

const BatteryLabel = () => Widget.Label({
  label: Battery.bind('percent').transform(p => `${p} %`),
});

const BatteryIcon = () => Widget.Icon({
  setup: self => self.hook(Battery, ic => {
    ic.icon = get_icon(Battery.percent, Battery.charging)
  }),
  size: 24
})

export default () => Widget.Box({
  children: [
    BatteryIcon(),
    BatteryLabel()
  ],
  class_name: Battery.bind('percent').transform(p => `battery ${colorise(p)}`),
  spacing: 8,
  visible: Battery.bind('available')
});


