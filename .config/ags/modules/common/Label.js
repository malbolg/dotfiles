export default (text, class_name) => Widget.Label({
  label: text,
  class_name: class_name
})
