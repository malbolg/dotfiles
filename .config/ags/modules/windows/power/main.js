import Clamp from '../../../assets/js/clamp.js'
import App from 'resource:///com/github/Aylur/ags/app.js'
import { POWERMENU_CONTENT, POWERMENU_CHILDREN_COUNT } from './contents.js'

const POWERMENU_WINDOW_NAME = 'power-menu'
export { POWERMENU_WINDOW_NAME };


const SELECTOR = Variable(0);

const Menu = () => Widget.EventBox({
  class_name: 'power-menu',
  hexpand: true,
  vpack: 'center',
  child: Widget.CenterBox({
    class_name: 'power-menu',
    centerWidget: Widget.Stack({
      children: POWERMENU_CONTENT,
      shown: SELECTOR.bind().transform(p => `${p}`),
      transition: 'slide_left_right'
    })
  })
})

export default () => Widget.Window({
  name: POWERMENU_WINDOW_NAME,
  class_name: 'power-menu-bg',
  visible: false,
  keymode: 'exclusive',
  exclusivity: "ignore",
  layer: "overlay",
  anchor: ['right', 'left', 'top', 'bottom'],
  child: Menu()
})
  .on("key-press-event", (_, event) => {
    switch (event.get_keycode()[1]) {
      case 113: { // LEFTARROW
        SELECTOR.setValue(
          Clamp(SELECTOR.getValue() - 1, 0, POWERMENU_CHILDREN_COUNT)
        )
        break;
      }
      case 114: { // RIGHTARROW
        SELECTOR.setValue(
          Clamp(SELECTOR.getValue() + 1, 0, POWERMENU_CHILDREN_COUNT)
        )
        break;
      }
      case 9: { // ESC
        SELECTOR.setValue(0)
        App.closeWindow(POWERMENU_WINDOW_NAME)
        break;
      }
    }
  })

// does not work somehow
//  .keybind("leftarrow", (_) => SELECTOR.setValue(
//    Clamp(SELECTOR.getValue() - 1, 0, POWERMENU_CHILDREN_COUNT)
//  ))
//  .keybind("rightarrow", (_) => SELECTOR.setValue(
//    Clamp(SELECTOR.getValue() + 1, 0, POWERMENU_CHILDREN_COUNT)
//  ))
//  .keybind("Escape", (_) => SELECTOR.setValue(0))

//ags -t <window-name>
