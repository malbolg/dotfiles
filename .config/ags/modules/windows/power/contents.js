import Utils from 'resource:///com/github/Aylur/ags/utils.js'
import App from 'resource:///com/github/Aylur/ags/app.js'
import { POWERMENU_WINDOW_NAME } from './main.js'


const SysButton = (label, icon, doafter) => Widget.Button({
  on_clicked: () => {
    App.closeWindow(POWERMENU_WINDOW_NAME)
    doafter()
  },
  class_name: 'power-menu-item',
  child: Widget.Box({
    class_name: 'power-menu-item-btn',
    vertical: true,
    children: [
      Widget.Icon({
        icon: icon,
        class_name: 'power-menu-item-icon'
      }),
      Widget.Label({
        label: label,
        class_name: 'power-menu-item-text'
      }),
    ],
  }),
});

const POWERMENU_CONTENT = {
  '0': SysButton('Shutdown', 'system-shutdown-symbolic', () => Utils.execAsync('systemctl poweroff')),
  '1': SysButton('Reboot', 'system-shutdown-symbolic', () => Utils.execAsync('systemctl reboot')),
  // ['2', SysButton('Logout', 'system-shutdown-symbolic')],
  '2': SysButton('Lock', 'system-lock-screen-symbolic', () => Utils.execAsync('swaylock'))
}

const POWERMENU_CHILDREN_COUNT = Object.keys(POWERMENU_CONTENT).length - 1;

export { POWERMENU_CONTENT, POWERMENU_CHILDREN_COUNT };
