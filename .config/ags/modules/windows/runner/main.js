const APPRUNNER_WINDOW_NAME = 'apprunner-window'
export { APPRUNNER_WINDOW_NAME }

// TODO

export default () => Widget.Window({
  name: APPRUNNER_WINDOW_NAME,
  exclusivity: "ignore",
  layer: "overlay",
  anchor: ["right", "top", "left", "bottom"],
  keymode: "exclusive",
  popup: true,
  visible: false,
  child: undefined
})
//.on("key-press-event", (_, ev) => {})
