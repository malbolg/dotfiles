//import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js'
//import Notifications from 'resource:///com/github/Aylur/ags/service/notifications.js'
//import Mpris from 'resource:///com/github/Aylur/ags/service/mpris.js'
//import Audio from 'resource:///com/github/Aylur/ags/service/audio.js'
//import Battery from 'resource:///com/github/Aylur/ags/service/battery.js'
//import SystemTray from 'resource:///com/github/Aylur/ags/service/systemtray.js'
//import Widget from 'resource:///com/github/Aylur/ags/widget.js'

import Utils from 'resource:///com/github/Aylur/ags/utils.js'
import App from 'resource:///com/github/Aylur/ags/app.js'
import TopBar from './modules/topbar/main.js'
import style_reload from './assets/js/style_reload.js'

import PowerWindow from './modules/windows/power/main.js'

let main_scss = `${App.configDir}/assets/stylesheet/scss/style.scss`
let scss_dir = `${App.configDir}/assets/stylesheet/scss`

Utils.monitorFile(scss_dir, () => { style_reload(main_scss) })
style_reload(main_scss)

export default {
  //  icons: App.configDir + '/assets/icons',
  // style: `${App.configDir}/assets/stylesheet/css/style.css`,
  windows: [TopBar(), PowerWindow()]

  //  closeWindowDelay: {'window-name': 500 /*ms*/ }, //useful for animations
  //  onConfigParsed: () => {},  // after the object is loaded
  //  onWindowToggled: () => {}, // on window visible
}
