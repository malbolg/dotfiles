import App from 'resource:///com/github/Aylur/ags/app.js';
import Utils from 'resource:///com/github/Aylur/ags/utils.js'

const css_path = `${App.configDir}/assets/stylesheet/css/style.css`

// The wiki recommands sassc, which is a wrapper for libsass, which is deprecated
// use dart-sass instead like the following
export default (style_root_path) => {
  Utils.execAsync(`sass ${style_root_path} ${css_path}`)
    .then(() => {
      App.resetCss()
      App.applyCss(css_path)
      console.log('style reloaded!')
    })
    .catch((reason) => { console.log(reason) })
}
