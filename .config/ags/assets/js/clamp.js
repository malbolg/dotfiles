export default (v, min, max) => Math.min(Math.max(v, min), max)
