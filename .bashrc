#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


#	[env]
. ~/.bash_env

#	[Alias]
. ~/.bash_alias

eval "$(starship init bash)"

